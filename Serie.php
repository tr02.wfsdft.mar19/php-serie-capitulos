<?php
require 'Temporada.php';

class Serie {
    private $nombre;
    private $genero;
    private $resumen_serie;
    public $temporada;

    public function __construct($nombre, $genero, $resumen_serie) {
        $this->nombre = $nombre;
        $this->genero = $genero;
        $this->resumen_serie = $resumen_serie;
        $this->temporada = [];
    }

    function get_titulo() {
        return $this->nombre;
    }

    public function get_numCapitulos($consultaTemporada) {
        foreach ($this->temporada as $valor)
        {
            if ($valor->get_temporada() == $consultaTemporada) {
                echo $valor->getCapitulosTemporada() .PHP_EOL;
            }
        }
    }

    public function getValoracionSerie() {
        $cont = 0;
        $sum = 0;
        foreach ($this->temporada as $valor) {
            $sum += $valor->getValoracionTemporada();
            $cont++;
        }
        return $sum/$cont;
    }

    public function addTemporada ($temporada) {
        array_push($this->temporada, $temporada);
    }
}