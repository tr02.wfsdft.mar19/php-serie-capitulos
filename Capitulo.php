<?php
require 'ElementoMultimedia.php';

class Capitulo extends ElementoMultimedia {
    private $nombreCapitulo;
    private $sinopsis;
    private $duracion;
    private $valoracion;

    public function __construct($nombreCapitulo, $sinopsis, $duracion, $valoracion) {
        $this->nombreCapitulo = $nombreCapitulo;
        $this->sinopsis = $sinopsis;
        $this->duracion = $duracion;
        $this->valoracion = $valoracion;
    }

    public function getValoracionCapitulo() {
        echo "Valoracion " . $this->valoracion .PHP_EOL;
        return $this->valoracion;
    }

    function get_capitulos() {
        return $this->nombreCapitulo;
    }

   
}