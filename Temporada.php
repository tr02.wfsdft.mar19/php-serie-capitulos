<?php

require 'Capitulo.php';

class Temporada {
    private $nombreTemporada;
    private $numCapitulos;
    public $capitulos;

    public function __construct($nombreTemporada, $numCapitulos) {
        $this->nombreTemporada = $nombreTemporada;
        $this->numCapitulos = $numCapitulos;
        $this->capitulos = [];
    }

    public function get_temporada() {
        return $this->nombreTemporada;
    }

    public function getValoracionTemporada() {
        $cont = 0;
        $sum = 0;
        if (sizeof($this->capitulos) == 0) { 
            return 0;
        }
        foreach ($this->capitulos as $valor) {
            if ($valor !=null) {
            $sum += $valor->getValoracionCapitulo();
            $cont++;
            }
        }
        return $sum/$cont;
    }

    public function getCapitulosTemporada () {
        return $this->numCapitulos;
    }

    public function get_numCapitulos($idCapitulo) {
        //$this->capitulo[$idCapitulo]->get

        return sizeof($capitulos);
    }

    public function addCapitulo ($capitulo) {
        array_push($this->capitulos, $capitulo);

        //$this->capitulos[$capitulo->id] = $capitulo;
    }
}